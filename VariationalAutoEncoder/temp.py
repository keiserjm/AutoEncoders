import torch
import torchvision
from torchvision import transforms

train_transform = transforms.Compose([
    transforms.ToTensor(),
])
